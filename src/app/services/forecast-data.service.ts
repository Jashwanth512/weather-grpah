import { Injectable,Inject } from '@angular/core';
import {HttpClient} from '@angular/common/http'
import {ChartComponent} from "../components/chart/chart.component"

@Injectable({
  providedIn: 'root'
})
export class ForecastDataService {

  istDates=[]
  temps=[]

// UTC TO IST Converter!!

pad(value) {
    return value > 9 ? value: "0" + value;
}

convertToist(utcValue)
{
var utc = utcValue;
var d = new Date(0); 
d.setUTCSeconds(utc);
var m = d.getUTCFullYear() + '-' + this.pad(d.getUTCMonth() + 1) + '-' + this.pad(d.getUTCDate()) 
        + ' ' + this.pad(d.getUTCHours()) + ':' + this.pad(d.getUTCMinutes())+ ':' + this.pad(d.getUTCSeconds());
var date=m.split(" ")
this.istDates.push(date[0])
}

// UTC TO IST Converter END!!

  daily_data:any={daily:[{dt:0,temp:{day:0}}]}
  preRequired_data:{
    coord:{lon:0,lat:0},
    name:""}
  cityName=""
  lat=0
  lon=0
  i=0
  constructor(@Inject(String) private dataFromReceiveService:any,private http:HttpClient,private cc?:ChartComponent)
  {
    this.preRequired_data=this.dataFromReceiveService
    this.cityName=this.preRequired_data.name
    this.lat=this.preRequired_data.coord.lat
    this.lon=this.preRequired_data.coord.lon
  }

  contactApi()
  {
  const url="https://api.openweathermap.org/data/2.5/onecall?lat="+this.lat.toString()+"&lon="+this.lon.toString()+"&appid=399da01a25cb7a0110a4cda2f992b5ec&units=metric"  
  return this.http.get(url)
  // console.log(fetch(url))
  }
  
  getForecastData()
  {
    this.contactApi().subscribe((data)=>
    {
      this.daily_data=data
      // console.log("7 DAY FORECAST: ",this.daily_data)
      console.log(this.daily_data.daily)
      for(this.i=0;this.i<8;this.i++)
      {
        var utc=this.daily_data.daily[this.i].dt
        this.convertToist(utc)
        var temp=this.daily_data.daily[this.i].temp.day
        this.temps.push(temp)
        if(this.temps.length==8 && this.istDates.length==8)
        {
            this.gotEnoughLength()
        }
      }
      // console.log(this.temps,this.istDates) 
    },(err)=>
    {
      console.log(err)
    })
  }

  gotEnoughLength()
  {
    console.log("gotEnoughLength")
    console.log(this.temps,this.istDates)
    this.cc.getChart()
    console.log("line 84")
  }

}
