import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import{ForecastDataService} from './forecast-data.service'
// import { NONE_TYPE } from '@angular/compiler';
// import {ChartComponent} from "../components/chart/chart.component"
@Injectable({
  providedIn: 'root',
})

export class ReceivedataService {
  cityData: any = {
    name: '',
    main: {
      temp: ''
    }
  };
  private url1 = 'https://api.openweathermap.org/data/2.5/weather?q=';
  private url2 = '&appid=d28274b5fe592e1f1e558103a5e66370&units=metric';
  constructor(public http: HttpClient) {}

  public fetchData(city) {
    const finalUrl = this.url1 + city + this.url2;
    return this.http.get(finalUrl);
  }

  public receiveData(requiredCity) {
    this.fetchData(requiredCity).subscribe((data) => {
      // console.log(data, 'data in api call');
      this.cityData = data;
      const k=new ForecastDataService(this.cityData,this.http)
      k.getForecastData()
      // this.fd(this.cityData)
    }, (err) => {
      console.log(err, 'err caught');
    });
  }
}
