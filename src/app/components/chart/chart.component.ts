import { Component, Inject, OnInit } from '@angular/core';
import {Chart,registerables} from "../../../../node_modules/chart.js"
import { ForecastDataService } from "../../services/forecast-data.service"
Chart.register(...registerables);
@Component({
  selector: 'app-chart',
  templateUrl: './chart.component.html',
  styleUrls: ['./chart.component.css'],
// providers:[ForecastDataService]
})
export class ChartComponent implements OnInit {
  
    private forecasrdataservice:ForecastDataService
    constructor()
    { }

  ngOnInit(): void {
}
getChart()
{
    console.log("Get Chart!")
    var myChart = new Chart("myChart", {
        type: 'bar',
        data: {
            labels:['Red', 'Blue', 'Yellow', 'Green', 'Purple', 'Orange','Red','Blue'],
            datasets: [{
                label: 'Temperature',
                data:[31.41, 26.43, 32.25, 31.69, 32.09, 32.47, 32.8, 33.03],
                backgroundColor: [
                    'rgba(255, 99, 132, 0.2)',
                    'rgba(54, 162, 235, 0.2)',
                    'rgba(255, 206, 86, 0.2)',
                    'rgba(75, 192, 192, 0.2)',
                    'rgba(153, 102, 255, 0.2)',
                    'rgba(255, 159, 64, 0.2)',
                    'rgba(153, 102, 255, 0.2)',
                    'rgba(255, 159, 64, 0.2)',
                ],
                borderColor: [
                    'rgba(255, 99, 132, 1)',
                    'rgba(54, 162, 235, 1)',
                    'rgba(255, 206, 86, 1)',
                    'rgba(75, 192, 192, 1)',
                    'rgba(153, 102, 255, 1)',
                    'rgba(255, 159, 64, 1)',
                    'rgba(153, 102, 255, 1)',
                    'rgba(255, 159, 64, 1)'
                ],
                borderWidth: 1
            }]
        },
        options: {
            scales: {
                y: {
                    beginAtZero: true
                }
            }
        }
    });
    // return 1;  
}
}
